<?php

class View_Helper_ValidatorForm extends Zend_View_Helper_Abstract
{
    public function validatorForm($form)
    {
        $arr = $form->getMessages();

        $html = '';
        if (count($arr) > 0){
            $html .= '<div style="color:red; text-align:center;" >';
            foreach ($arr as $key => $arrValue) {
                foreach ($arrValue as $value) {
                    $html .= '<strong>'.$form->getElement($key)->getLabel().':</strong> '.$value . '<br>';
                }
            }
            $html .= '<br></div>';
        }
        return $html;
    }
}
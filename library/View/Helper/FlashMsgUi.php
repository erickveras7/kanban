<?php

class View_Helper_FlashMsgUi extends Zend_View_Helper_Abstract
{
    public function flashMsgUi($tipo = 'info', $msg = null, $esconder = true, $tempo = '4000', $div_render = '$("#msg-alert")') {

        $esconder = ($esconder == true) ? 'true' : 'false';

        $msg = str_replace("\r\n",";",trim($msg));
        $msg = preg_replace('/(\'|")/', '', $msg);
        $msg = str_replace("\n", "<br>", $msg);
        
        $html  = '<script type="text/javascript">';
        $html .= "alertMsg('{$msg}', '{$tempo}', '{$tipo}', {$div_render}, {$esconder});";
        $html .= '</script>';
        
        return $html;
    }

}
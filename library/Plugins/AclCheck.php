<?php

class Plugins_AclCheck extends Zend_Controller_Plugin_Abstract {

    protected $_auth = null;
    protected $_acl = null;
    protected $_forbiddenRoute = array(
        'controller' => 'error',
        'action' => 'denied',
        'module' => 'default'
    );

    /**
     * Grupo helpdesk tem permissão a todas as funcionalidades da web
     *
     * @author Jannier Magalhães
     * @param Zend_Controller_Request_Abstract $request
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request) {

        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();

        $request->setControllerName($controller);
        $request->setActionName($action);
        $request->setModuleName($module);
    }

    /**
     * Verifica se o grupo do usuario esta habilitado a acessar a funcionalidade
     * requisitada
     *
     * @author Jannier Magalhães / Alan Bacelar
     *
     * @param string $module
     * @param string $controller
     * @param string $action
     * @return bool
     */
//    protected function _isAuthorized($module, $controller, $action) {
//        $arrDadosUsuario = $this->_auth->getIdentity();
//        $funcionalidade = ($module == 'default') ? $controller : "{$module}:{$controller}";
//
//        // verifica permissao no controller ou na action do controller
//        if (!$this->_acl->has($funcionalidade) || !$this->_acl->isAllowed($arrDadosUsuario->papeis[0]['s_descricao'], $funcionalidade, $action)) {
//            return false;
//        }
//
//        return true;
//    }

}

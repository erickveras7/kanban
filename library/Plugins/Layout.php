<?php
/**
 * Responsável por definir o layout correto conforme modulo escolhido
 */
class Plugins_Layout extends Zend_Controller_Plugin_Abstract
{
    /**
     *
     * @param Zend_Controller_Request_Abstract $request
     */
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('layout')
               ->setLayoutPath( APPLICATION_PATH . '/layout');
    }
}
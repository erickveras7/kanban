<?php
/**
 * Verifica se o usaurio esta logado no sistema
 */
class Plugins_CheckHasAccess extends Zend_Controller_Plugin_Abstract
{
    public function routeShutdown( Zend_Controller_Request_Abstract $request ){ 
        $controller = 'login';
        $action = 'index';

        $request->setControllerName($controller);
        $request->setActionName($action);
    }
}

function dialogConfirmFunction(titulo, msg, funcao, tipo){

    var janela = $("<div id='div_alerta'></div>");

    janela.dialog({
        title: titulo,
        modal: true,
        close : function(){
            $(this).dialog().remove();
        },
        buttons : {
            Sim : function(){
                $("#div_alerta").dialog('close');
                $(this).dialog().remove();
                funcao();
            },
            Não : function(){
                $("#div_alerta").dialog('close');
                $(this).dialog().remove();
            }
        }
    });

    switch(tipo)
    {
        case 'excluir':
            janela.html('<p><span class="ui-icon-confirmacao" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p>');
            break;
        case 'info':
            janela.html('<p><span class="ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p>');
            break;
        case 'confirm':
            janela.html('<p><span class="ui-icon-confirmacao" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p>');
            break;
        case 'alert':
            janela.html('<p><span class="ui-icon-atencao" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p>');
            break;
        case 'erro':
            janela.html('<p><span class="ui-icon-erro" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p>');
            break;
        case 'sair':
            janela.html('<p><span class="ui-icon ui-icon-closethick" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p>');
            break;
        default:
            janela.html('<p><span class="ui-icon-confirmacao" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p>');
            break;
    }
}

function alertMsg(msg, tempo, tipo, div, esconder){

    if(tempo == undefined) tempo = '4000';
    if(tipo == undefined) tipo = 'info';
    if(div == undefined) div = $('#msg-alert');
    if(esconder == undefined) esconder = (tipo == 'erro') ? false : true;

    if( $('#msg_info', div) != undefined ) {
       $('#msg_info', div).remove();
    }

    var classe_info = 'ui-state-highlight';
    var classe_tipo = 'ui-icon-circle-check';

    switch (tipo) {
        case 'info':
            classe_tipo = 'ui-icon-circle-check';
            break;

        case 'erro':
            classe_info = 'ui-state-error';
            classe_tipo = 'ui-icon-cancel';
            break;

        case 'alert':
            classe_tipo = 'ui-icon-alert';
            break;

        case 'block':
            classe_tipo = 'ui-icon-locked';
            break;

        default:
            break;
    }

    $(div).append('<div id="msg_info" title="Dê um duplo clique para fechar essa mensagem." class="'+classe_info+' ui-corner-all" style="display: none;"><span class="ui-icon '+classe_tipo+'" style="float:left; margin:0 7px 0px 0;"></span><span style="color:blue;">'+msg+'</span></div>');
    $('#msg_info', div).fadeIn(800);

    var tam = $('#msg_info', div).width();
    $('#msg_info', div).css('margin-left', - (tam / 2));

    if(esconder == true || esconder == 'true') {
        $('#msg_info', div).delay(tempo).fadeOut(500);
    }

    $('#msg_info', div).dblclick(function(){
            $(this).remove();
    });

}

function requestAjax(url, params, method, div, delete_div_content, after_success){

    if(delete_div_content == undefined) delete_div_content = true;
    if(method == undefined) method = 'GET';
    if(div == undefined) div = '#conteudo';

    $.ajax({
        type: method,
        url : url,
        data : params,
        success: function(o){
            if(delete_div_content == true) {
                $(div).html(o);
            } else {
                $(div).append(o);
            }

            if(typeof(after_success) != 'undefined'){
                after_success(o);
            }
        },
        error: function(o){
            console.log(o);
        }
    });
}

function atualizaContent(url){
    requestAjax(url, {}, 'GET', '#conteudo');
}
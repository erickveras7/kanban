<?php
/**
 * Define o cabeçalho padrão de requisição dos arquivos...
 */
if(version_compare(phpversion(), 5) === '-1'){
	die('Necessario ter PHP 5 ou superior');
}

if (!extension_loaded('pdo')) {
	die('necessario o modulo PDO');
}


if (!extension_loaded('pdo_pgsql')) {
	die('necessario o modulo pdo_pgsql');
}

//if (!extension_loaded('pdo_firebird')) {
//	die('necessario o modulo pdo_firebird');
//}

date_default_timezone_set('America/Fortaleza');
setlocale(LC_ALL, 'pt_BR');

/**
* Define o diretorio onde a aplicação está rodando.
* Se não tiver definido anteriormente, define usando realpath(dirname(__FILE__) . '/../application').
* O resultado será /var/www/html/palm3/application
*/
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

/**
* Define o ambiente da aplicação
* Assume 'production' como ambiente padrão se a variável APPLICATION_ENV nao tiver sido setada no arquivo .htaccess.
*/
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath('/var/www/web_library/'),
    get_include_path()
)));


/** Zend_Application */
require_once 'Zend/Application.php';  

/**
* Cria uma instância de Zend_Application passando o ambiente e o caminho para o arquivo de configurações.
* Este componente irá automatizar algumas configurações definidas no config.ini
*/
$application = new Zend_Application(
    APPLICATION_ENV, 
    APPLICATION_PATH . '/configs/application.ini'
);



//Faz as configuraçoes basicas (Views, Layout, DB, ...)
$application->bootstrap()
            ->run();//Inicia efetivamente a aplicacao. Neste momento é dado o dispatch para instanciar os controllers e chamar os actions

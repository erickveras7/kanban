<?php
$portugues = array(
    'isEmpty'                       => 'Este campo é obrigatório.',
    'invalidUrl'                    => "'%value%' não é uma URL válida",
    'stringEmpty'                   => "'%value%' é um texto vazio",
    'recordFound'                   => 'Um registro com o valor %value% já foi cadastrado',
    'notFloat'                      => "O número '%value%' não é válido. Certifique-se de não informar vírgulas e sim pontos",

    //data
    'dateNotYYYY-MM-DD'             => "'%value%' não está no formato AAAA-MM-DD",
    'dateInvalidDate'               => "'%value%' não é uma data válida",
    'dateInvalid'                   => "'%value%' não parece ser um data válida",
    'dateFalseFormat'               => "'%value%' não corresponde a um formato de data válido",

    //arquivos
    'fileUploadErrorNoFile'         => "Arquivo não foi carregado",
    'fileUploadErrorFormSize'       => "Arquivo maior que o tamanho permitido",
    'fileExtensionFalse'            => "Tipo de arquivo não permitido",
    'fileUploadErrorIniSize'        => "Arquivo maior que o tamanho permitido para carregar pelo servidor",

    // Email
    'emailAddressInvalid'           => 'Não é um email válido',
    'emailAddressInvalidFormat'     => 'Não é um email válido',
    'emailAddressInvalidHostname'   => "'%hostname%' não é hostname válido para o endereço de e-mail '%value%'",

    //hostname
    'hostnameIpAddressNotAllowed'   => "'%value%' Parece ser um endereço de IP, mas endereços de IP não são permitidos",
    'hostnameUnknownTld'            => "'%value%' parece ser um DNS, mas não foi possivel validar o TLD",
    'hostnameDashCharacter'         => "'%value%' parece ser um DNS, mas contém um 'dash' (-) em uma posição inválida",
    'hostnameInvalidHostnameSchema' => "'%value%' parece ser um DNS, mas não foi possível comparar com o schema para o TLD '%tld%'",
    'hostnameUndecipherableTld'     => "'%value%' parece ser um DNS mas não foi possível extrair o TLD",
    'hostnameInvalidHostname'       => "'%value% não é compatível com a estrutura DNS",
    'hostnameInvalidLocalName'      => "'%value%' não parece ser uma rede local válida",
    'hostnameLocalNameNotAllowed'   => "'%value%' parece ser o nome de uma rede local, mas nome de rede local não são permitido",

    //identical
    'notSame'                       => "Comparação não bate",
    'missingToken'                  => "Não foi fornecido parâmetros para teste",

    //greater then
    'notGreaterThan'                => "'%value%' não á maior que '%min%'",

    //digits
    'notDigits'                     => "Permitido somente números",
    'digitsStringEmpty'             => "Este campo é obrigatório e deve ser preenchido",

    //between
    'notBetween'                    => "'%value%' não está entre '%min%' e '%max%', inclusive",
    'notBetweenStrict'              => "'%value%' não está estritamente entre '%min%' e '%max%'",

    //alnum
    'notAlnum'                      => "'%value%' não possuí apenas letras e dígitos",

    //alpha
    'notAlpha'                      => "'%value%' não possuí apenas letras",

    //in array
    'notInArray'                    => "'%value%' não foi encontrado na lista",

    //int
    'notInt'                        => "'%value%' não parece ser um inteiro",

    //ip
    'notIpAddress'                  => "'%value%' não parece ser um endereço ip válido",

    //lessthan
    'notLessThan'                   => "'%value%' não é menor que '%max%'",

    //regex
    'regexNotMatch'                 => "'%value%' não foi validado na expressão '%pattern%'",

    //stringlength
    'stringLengthTooShort'          => "é menor que %min% (tamanho mínimo desse campo)",
    'stringLengthTooLong'           => "Não pode conter mais de %max% caracteres.",

//    Captcha
    'badCaptcha'                    => "O valor digitado está errado."
);
return $portugues;
?>
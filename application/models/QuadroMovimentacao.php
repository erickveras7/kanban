<?php

class Model_QuadroMovimentacao
{
    private $_db;
    private $_base;

    public function __construct($base = 'kanban') {
        $this->_base = $base;
        $this->_db = new Model_DbTable_QuadroMovimentacao(Zend_Registry::get($base));
    }

    public function getDb() {
        return $this->_db;
    }
    
    public function getAdapter() {
        return $this->_db->getAdapter();
    }

    public function getQuadroMovimentacoes(){
        return $this->getAdapter()->fetchAll(
            "SELECT atividade_de
                  , situacao_de
                  , atividade_para
                  , situacao_para
                  FROM t_quadro_movimentacao ORDER BY 1,2,3,4;"
        );
    }
}

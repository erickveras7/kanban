<?php

class Model_Situacao
{
    private $_db;
    private $_base;

    public function __construct($base = 'kanban') {
        $this->_base = $base;
        $this->_db = new Model_DbTable_Situacao(Zend_Registry::get($base));
    }

    public function getDb() {
        return $this->_db;
    }
    
    public function getAdapter() {
        return $this->_db->getAdapter();
    }

    public function getSituacoes(){
        return $this->getAdapter()->fetchAll("SELECT id_situacao, descricao FROM t_situacao ORDER BY descricao;");
    }

    public function getSituacoesQuadro(){
        return $this->getAdapter()->fetchAll("SELECT id_situacao, descricao FROM t_situacao WHERE b_quadro_visivel = true ORDER BY id_situacao;");
    }
    
    public function getSituacao($id){
        return $this->getAdapter()->fetchRow(
            "SELECT id_situacao
                  , descricao
              FROM t_situacao WHERE id_projeto = :id ORDER BY 1;", array('id' => $id)
        );
    }

}

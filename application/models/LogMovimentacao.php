<?php

class Model_LogMovimentacao
{
    private $_db;
    private $_base;

    public function __construct($base = 'kanban') {
        $this->_base = $base;
        $this->_db = new Model_DbTable_LogMovimentacao(Zend_Registry::get($base));
    }

    public function getDb() {
        return $this->_db;
    }
    
    public function getAdapter() {
        return $this->_db->getAdapter();
    }

    public function addMovimentacao($movimentacao){
        return $this->getAdapter()->query(
            "INSERT INTO t_log_movimentacao(id_tarefa, id_situacao_inicial, id_situacao_final, data_hora_mov, id_atividade_inicial, id_atividade_final, id_autor, id_apelido)
            VALUES(:id_tarefa, :id_situacao_inicial, :id_situacao_final, now(), :id_atividade_inicial, :id_atividade_final, :id_autor, :id_apelido);", $movimentacao);
    }

    public function getLogMovimentacoes(){
        return $this->getAdapter()->fetchAll(
            "SELECT t1.id_tarefa, 
                    t2.descricao AS situacao_inicial,
                    t3.descricao AS situacao_final,  
                    t4.descricao AS atividade_inicial, 
                    t5.descricao AS atividade_final, 
                    t1.id_autor, 
                    t1.id_apelido,
                    TO_CHAR(t1.data_hora_mov, 'YYYY-MM-DD HH24:MI:SS')
               FROM t_log_movimentacao t1 
               LEFT JOIN t_situacao t2 ON (t1.id_situacao_inicial = t2.id_situacao)
               LEFT JOIN t_situacao t3 ON (t1.id_situacao_final = t3.id_situacao)
               LEFT JOIN t_atividade t4 ON (t1.id_atividade_inicial = t4.id_atividade)
               LEFT JOIN t_atividade t5 ON (t1.id_atividade_final = t5.id_atividade)
               ORDER BY t1.data_hora_mov DESC;");
    }

}

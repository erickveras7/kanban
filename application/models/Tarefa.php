<?php
/*
 *   id_tarefa serial NOT NULL,
  descricao character varying(16) NOT NULL,
  id_apelido character varying(16),
  id_projeto integer,
  id_tipo_tarefa integer NOT NULL,
  id_atividade integer NOT NULL,
  id_situacao integer NOT NULL,
  CONSTRAINT t_tarefa_pkey PRIMARY KEY (id_tarefa)
 */
class Model_Tarefa
{
    private $_db;
    private $_base;

    public function __construct($base = 'kanban') {
        $this->_base = $base;
        $this->_db = new Model_DbTable_Tarefa(Zend_Registry::get($base));
    }

    public function getDb() {
        return $this->_db;
    }
    
    public function getAdapter() {
        return $this->_db->getAdapter();
    }

    public function getTarefas(){
        return $this->getAdapter()->fetchAll(
          'SELECT t1.id_tarefa AS "Id"
                , t1.assunto AS "Assunto"
                , t1.descricao AS "Descrição"
                , t2.descricao AS "Projeto"
                , t1.id_apelido AS "Responsável"
                , t6.descricao AS "Tipo"
                , t4.descricao AS "Atividade"
                , t5.descricao AS "Situação"
             FROM t_tarefa t1
             JOIN t_projeto t2 ON (t1.id_projeto = t2.id_projeto)
             LEFT JOIN t_usuario t3 ON (t1.id_apelido = t3.id_apelido_usuario)
             LEFT JOIN t_atividade t4 ON (t1.id_atividade = t4.id_atividade)
             JOIN t_situacao t5 ON (t1.id_situacao = t5.id_situacao)
             JOIN t_tipo_tarefa t6 ON (t1.id_tipo_tarefa = t6.id_tipo_tarefa);'
        );
    }

    public function getTarefasPorProjeto($id_projeto){
        return $this->getAdapter()->fetchAll(
          'SELECT t1.id_tarefa AS "Id"
                , t1.assunto AS "Assunto"
                , t1.descricao AS "Descrição"
                , t2.descricao AS "Projeto"
                , t1.id_apelido AS "Responsável"
                , t6.descricao AS "Tipo"
                , t4.descricao AS "Atividade"
                , t5.descricao AS "Situação"
                , t1.id_situacao AS id_situacao
                , t1.id_atividade AS id_atividade
             FROM t_tarefa t1
             JOIN t_projeto t2 ON (t1.id_projeto = t2.id_projeto)
             LEFT JOIN t_usuario t3 ON (t1.id_apelido = t3.id_apelido_usuario)
             LEFT JOIN t_atividade t4 ON (t1.id_atividade = t4.id_atividade)
             JOIN t_situacao t5 ON (t1.id_situacao = t5.id_situacao)
             JOIN t_tipo_tarefa t6 ON (t1.id_tipo_tarefa = t6.id_tipo_tarefa)
            WHERE t1.id_projeto = :projeto;', array('projeto' => $id_projeto)
        );
    }
    
    public function getTarefa($id){
        return $this->getAdapter()->fetchRow(
            'SELECT t1.id_tarefa AS "Id"
                , t1.assunto AS "Assunto"
                , t1.descricao AS "Descrição"
                , t2.id_projeto AS "Projeto"
                , t1.id_apelido AS "Responsável"
                , t6.id_tipo_tarefa AS "Tipo"
                , t4.id_atividade AS "Atividade"
                , t5.id_situacao AS "Situação"
             FROM t_tarefa t1
             JOIN t_projeto t2 ON (t1.id_projeto = t2.id_projeto)
             LEFT JOIN t_usuario t3 ON (t1.id_apelido = t3.id_apelido_usuario)
             LEFT JOIN t_atividade t4 ON (t1.id_atividade = t4.id_atividade)
             JOIN t_situacao t5 ON (t1.id_situacao = t5.id_situacao)
             JOIN t_tipo_tarefa t6 ON (t1.id_tipo_tarefa = t6.id_tipo_tarefa) WHERE t1.id_tarefa = :id;', array('id' => $id)
        );
    }
    
    public function excluirTarefas($id){
        return $this->getAdapter()->query("DELETE FROM t_tarefa WHERE id_tarefa = ANY ( ('{' || :id || '}')::int[] );", array('id' => $id));
    }
    
    public function adicionarTarefa($tarefa){
        return $this->getAdapter()->query("INSERT INTO t_tarefa(assunto, id_apelido, id_projeto, id_tipo_tarefa, id_atividade, id_situacao, descricao)
    VALUES (:assunto, :apelido, :projeto, :tipo, :atividade, :situacao, :descricao);", $tarefa);
    }

    public function updateTarefa($tarefa){
        return $this->getAdapter()->query("UPDATE t_tarefa SET assunto = :assunto, descricao = :descricao, id_projeto = :projeto, id_apelido = :apelido , id_tipo_tarefa = :tipo, id_atividade = :atividade
                                            WHERE id_tarefa = :id;", $tarefa);
    }

    public function updateTarefaQuadro($tarefa){
        return $this->getAdapter()->query("UPDATE t_tarefa SET id_situacao = :situacao, id_atividade = :atividade
                                            WHERE id_tarefa = :id;", $tarefa);
    }
}

<?php

class Model_Usuario
{
    private $_db;
    private $_base;

    public function __construct($base = 'kanban') {
        $this->_base = $base;
        $this->_db = new Model_DbTable_Usuario(Zend_Registry::get($base));
    }

    public function getDb() {
        return $this->_db;
    }
    
    public function getAdapter() {
        return $this->_db->getAdapter();
    }

    public function validaUsuario($usuario)
    {
        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authAdapter->setTableName('t_usuario')
                ->setIdentityColumn('login')
                ->setCredentialColumn('senha')
                ->setCredentialTreatment('MD5(?)')
                ->setIdentity($usuario['usuario'])
                ->setCredential($usuario['senha']);        
        
        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session('kanban'));

        if ($auth->authenticate($authAdapter)->isValid()) {
            $dadosUser = $authAdapter->getResultRowObject(null, 'senha'); // dados do usuario sem a senha
            $auth->getStorage()->write($dadosUser);
            return true;
        }

        return false;
    }

    public function getUsuarios(){
        return $this->getAdapter()->fetchAll(
            "SELECT id_apelido_usuario
                  , nome
                  , login FROM t_usuario ORDER BY id_apelido_usuario;"
        );
    }
    
    public function getUsuario($id){
        return $this->getAdapter()->fetchRow(
            "SELECT id_apelido_usuario, nome, login FROM t_usuario WHERE id_apelido_usuario = :id;", array('id' => $id)
        );
    }
    
    public function excluirUsuarios($id){
        return $this->getAdapter()->query("DELETE FROM t_usuario WHERE id_apelido_usuario = ANY ( ('{' || :id || '}')::text[] )", array('id' => $id));
    }
    
    public function adicionarUsuario($usuario){
        return $this->getAdapter()->query("INSERT INTO t_usuario(id_apelido_usuario, nome, login, senha) VALUES (:apelido, :nome, :login, md5(:senha));", $usuario);
    }

    public function updateUsuario($usuario, $apelido_anterior){
        
        $usuario['apelido_anterior'] = $apelido_anterior;
        
        if ($usuario['senha'] != ''){
            return $this->getAdapter()->query("UPDATE t_usuario SET id_apelido_usuario = :apelido, nome = :nome, login = :login, senha = md5(:senha)
                                                WHERE id_apelido_usuario = :apelido_anterior;", $usuario);
        } else {
            unset($usuario['senha']);
            return $this->getAdapter()->query("UPDATE t_usuario SET id_apelido_usuario = :apelido, nome = :nome, login = :login
                                                WHERE id_apelido_usuario = :apelido_anterior;", $usuario);
        }
    }

}

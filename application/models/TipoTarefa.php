<?php

class Model_TipoTarefa
{
    private $_db;
    private $_base;

    public function __construct($base = 'kanban') {
        $this->_base = $base;
        $this->_db = new Model_DbTable_TipoTarefa(Zend_Registry::get($base));
    }

    public function getDb() {
        return $this->_db;
    }
    
    public function getAdapter() {
        return $this->_db->getAdapter();
    }

    public function getTipos(){
        return $this->getAdapter()->fetchAll(
            "SELECT id_tipo_tarefa
                  , descricao
                  FROM t_tipo_tarefa ORDER BY descricao;"
        );
    }
    
    public function getTipo($id){
        return $this->getAdapter()->fetchRow(
            "SELECT id_tipo_tarefa
                  , nome
                  , descricao
                  , status
              FROM t_tipo_tarefa WHERE id_tipo_tarefa = :id;", array('id' => $id)
        );
    }
    
    public function excluirTipos($id){
        return $this->getAdapter()->query("DELETE FROM t_tipo_tarefa WHERE id_tipo_tarefa = ANY ( ('{' || :id || '}')::int[] )", array('id' => $id));
    }
    
    public function adicionarTipo($tipo){
        return $this->getAdapter()->query("INSERT INTO t_tipo(nome,descricao,status) VALUES (:nome, :descricao, true);", $tipo);
    }

    public function updateTipo($tipo){
        return $this->getAdapter()->query("UPDATE t_tipo_tarefa SET descricao = :descricao WHERE id_tipo_tarefa = :id;", $tipo);
    }

}

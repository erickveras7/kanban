<?php

class Model_Projeto
{
    private $_db;
    private $_base;

    public function __construct($base = 'kanban') {
        $this->_base = $base;
        $this->_db = new Model_DbTable_Projeto(Zend_Registry::get($base));
    }

    public function getDb() {
        return $this->_db;
    }
    
    public function getAdapter() {
        return $this->_db->getAdapter();
    }

    public function getProjetos(){
        return $this->getAdapter()->fetchAll(
            "SELECT id_projeto
                  , nome
                  , descricao
                  , CASE WHEN status IS TRUE THEN 'Ativo' ELSE 'Inativo' END FROM t_projeto ORDER BY nome;"
        );
    }
    
    public function getProjeto($id){
        return $this->getAdapter()->fetchRow(
            "SELECT id_projeto
                  , nome
                  , descricao
                  , status
              FROM t_projeto WHERE id_projeto = :id;", array('id' => $id)
        );
    }
    
    public function excluirProjetos($id){
        return $this->getAdapter()->query("DELETE FROM t_projeto WHERE id_projeto = ANY ( ('{' || :id || '}')::int[] )", array('id' => $id));
    }
    
    public function adicionarProjeto($projeto){
        return $this->getAdapter()->query("INSERT INTO t_projeto(nome,descricao,status) VALUES (:nome, :descricao, true);", $projeto);
    }

    public function updateProjeto($projeto){
        return $this->getAdapter()->query("UPDATE t_projeto SET descricao = :descricao, status = :status, nome = :nome
                                            WHERE id_projeto = :id;", $projeto);
    }

}

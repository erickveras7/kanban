<?php

class Model_Atividade
{
    private $_db;
    private $_base;

    public function __construct($base = 'kanban') {
        $this->_base = $base;
        $this->_db = new Model_DbTable_Atividade(Zend_Registry::get($base));
    }

    public function getDb() {
        return $this->_db;
    }
    
    public function getAdapter() {
        return $this->_db->getAdapter();
    }

    public function getAtividades(){
        return $this->getAdapter()->fetchAll(
            "SELECT id_atividade, descricao FROM t_atividade ORDER BY id_atividade;"
        );
    }
    
    public function getAtividade($id){
        return $this->getAdapter()->fetchRow(
            "SELECT id_atividade
                  , descricao
              FROM t_atividade WHERE id_atividade = :id;", array('id' => $id)
        );
    }
    
    public function excluirAtividades($id){
        return $this->getAdapter()->query("DELETE FROM t_atividade WHERE id_atividade = ANY ( ('{' || :id || '}')::int[] )", array('id' => $id));
    }
    
    public function adicionarAtividade($descricao){
        return $this->getAdapter()->query("INSERT INTO t_atividade(descricao) VALUES (:descricao);", $descricao);
    }

    public function updateAtividade($atividade){
        return $this->getAdapter()->query("UPDATE t_atividade SET descricao = :descricao
                                            WHERE id_atividade = :id;", $atividade);
    }

}

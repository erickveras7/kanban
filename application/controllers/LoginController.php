<?php

class LoginController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->disableLayout();
    }

    /**
     * Método responsável por realizar o login
     */
    public function indexAction() {
        if ( isset( $_SERVER["HTTPS"] ) && strtolower( $_SERVER["HTTPS"] ) != "on" ) {
            $this->_redirect("https://" . $_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']);
        }

        $form = new Form_Login();
        $this->view->form = $form;
        $this->view->headTitle('Login Kanban');

        $request = $this->getRequest();

        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $usuario = $form->getValue('usuario');
                $senha = $form->getValue('senha');

                try {
                    $model_usuario = new Model_Usuario();
                    $usuario_valido = $model_usuario->validaUsuario(array('usuario' => $usuario, 'senha' => $senha));

                    if (!$usuario_valido) {
                        throw new Zend_Exception('Login invalido');
                    } else {
                        $auth = Zend_Auth::getInstance();
                        $auth->setStorage(new Zend_Auth_Storage_Session('kanban'));
                        $dados_usuario = $auth->getStorage()->read();
                        $encodingBanco = Zend_Db_Table::getDefaultAdapter()->fetchRow('SELECT getdatabaseencoding() as encodingBanco;', array(), Zend_DB::FETCH_ASSOC);

                        $session = new Zend_Session_Namespace('session_kanban');
                        $session->usuario = $dados_usuario;
                        $session->encodingbanco = $encodingBanco['encodingbanco'];
                        $session->dbAdapters = Zend_Registry::get('kanban');

                        $this->_redirect('http://'.$_SERVER['SERVER_NAME'] . $this->getRequest()->getBaseUrl());
                    }
                } catch (Zend_Exception $e) {
                    $this->view->msg = 'Usu&aacute;rio ou senha inv&aacute;lida';
                    self::destroiDadosSessao();
                }
            }
        }
    }

    public function logoutAction() {
        self::destroiDadosSessao();
        $this->_redirect(Zend_Controller_Front::getInstance()->getBaseUrl()."/../");
    }

    private function destroiDadosSessao() {
        Zend_Auth::getInstance()->clearIdentity();
    }
}

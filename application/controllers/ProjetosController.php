<?php

class ProjetosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function indexAction()
    {
        $model_projeto  = new Model_Projeto();
        $projetos = $model_projeto->getProjetos();

        $this->view->projetos = $projetos;
    }

    public function excluirAction(){
        $request = $this->getRequest();

        if($request->isPost()) {
            $dados_post = $request->getParams();
            $model_projeto = new Model_Projeto();
            $model_projeto->excluirProjetos($dados_post['projeto']);
            $this->view->msg = array('info', 'Projeto ' . $dados_post['projeto'] . ' excluido com sucesso');
        }
    }

    public function editarAction(){
        $request = $this->getRequest();
        $dados = $request->getParams();

        $model_projeto  = new Model_Projeto();

        if($request->isPost()) {
            
            $projeto = array(
                'id'    => $dados['id_projeto'],
                'nome'      => $dados['nome'],
                'descricao' => $dados['descricao'],
                'status'    => (isset($dados['status'])) ? $dados['status'] : 0
            );
            
            $model_projeto = new Model_Projeto();
            $model_projeto->updateProjeto($projeto);
            
            $this->view->msg = array('info', 'Projeto editado com sucesso');
        } else {
            $projeto = $model_projeto->getProjeto($dados['projeto']);
            
            if ($projeto['status']){
                $projeto['status'] = 'checked';
            } else {
                $projeto['status'] = '';
            }
            
            $this->view->projeto = $projeto;
        }
    }

    public function adicionarAction(){
        $request = $this->getRequest();

        if($request->isPost()) {
            $dados_post = $request->getParams();

            $projeto = array(
                'nome'      => $dados_post['nome'],
                'descricao' => $dados_post['descricao']
            );

            $model_projeto = new Model_Projeto();
            $model_projeto->adicionarProjeto($projeto);
        }
        
        $this->view->msg = array('info', 'Projeto ' . $dados_post['nome'] . ' adicionado com sucesso');
    }
}
<?php

class TarefasController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function indexAction()
    {
        $model_tarefa  = new Model_Tarefa();
        $tarefas = $model_tarefa->getTarefas();

        $this->view->tarefas = $tarefas;
    }

    public function excluirAction(){
        $request = $this->getRequest();

        if($request->isPost()) {
            $dados_post = $request->getParams();
            $model_tarefa = new Model_Tarefa();
            $model_tarefa->excluirTarefas($dados_post['tarefa']);
            $this->view->msg = array('info', 'Tarefa ' . $dados_post['tarefa'] . ' excluido com sucesso');
        }
    }

    public function editarAction(){
        $request = $this->getRequest();
        if($request->isPost()) {
            $dados = $request->getParams();

            $tarefa = array(
                'id'    => $dados['tarefa'],
                'assunto' => $dados['assunto'],
                'descricao' => $dados['descricao'],
                'projeto' => $dados['projeto'],
                'apelido' => $dados['apelido'],
                'tipo' => $dados['tipo'],
                'atividade' => $dados['atividade']
            );
            
            $model_tarefa = new Model_Tarefa();
            $model_tarefa->updateTarefa($tarefa);
            
            $this->view->msg = array('info', 'Tarefa editado com sucesso');
        }
    }
    
    public function janelaEditarAction(){
        $request = $this->getRequest();
        $dados = $request->getParams();

        $model_tarefa  = new Model_Tarefa();
        $model_usuario = new Model_Usuario();
        $model_projeto = new Model_Projeto();
        $model_tipo = new Model_TipoTarefa();
        $model_atividade = new Model_Atividade();

        $this->view->tarefa = $model_tarefa->getTarefa($dados['tarefa']);
        $this->view->usuarios   = $model_usuario->getUsuarios();
        $this->view->projetos   = $model_projeto->getProjetos();
        $this->view->atividades = $model_atividade->getAtividades();
        $this->view->tipos      = $model_tipo->getTipos();
    }

    public function janelaAdicionarAction(){
        $model_usuario = new Model_Usuario();
        $model_projeto = new Model_Projeto();
        $model_tipo = new Model_TipoTarefa();
        $model_atividade = new Model_Atividade();

        $this->view->usuarios   = $model_usuario->getUsuarios();
        $this->view->projetos   = $model_projeto->getProjetos();
        $this->view->atividades = $model_atividade->getAtividades();
        $this->view->tipos      = $model_tipo->getTipos();
        
    }

    public function adicionarAction(){
        $request = $this->getRequest();

        if($request->isPost()) {
            $dados_post = $request->getParams();

            $tarefa = array(
                'assunto'   => $dados_post['nome'],
                'descricao' => $dados_post['descricao'],
                'projeto' => $dados_post['projeto'],
                'apelido' => $dados_post['apelido'],
                'tipo' => $dados_post['tipo'],
                'atividade' => $dados_post['atividade'],
                'situacao' => 1 // inicia-se com a situacao criada
            );

            $model_tarefa = new Model_Tarefa();
            $model_tarefa->adicionarTarefa($tarefa);
            
            $this->view->msg = array('info', 'Tarefa adicionada com sucesso');
        }
    }
}
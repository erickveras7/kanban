<?php

class QuadroController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $dados = $request->getParams();
        $projeto = $dados['params'];

        $model_atividade  = new Model_Atividade();
        $model_situacao = new Model_Situacao();
        $model_tarefa = new Model_Tarefa();
        $model_quadro = new Model_QuadroMovimentacao();
        $movimentacoes = $model_quadro->getQuadroMovimentacoes();
        
        /*
            array [
                'atividade_1_situacao_1' => array[
                    0 => atividade_1_situacao_2
                ]
            ]

         * Exemplo:
            array (size=23)
              'atividade_1_situacao_2' => 
                array (size=1)
                  0 => string 'atividade_1_situacao_1' (length=22)
              'atividade_1_situacao_3' => 
                array (size=1)
                  0 => string 'atividade_1_situacao_2' (length=22)
              'atividade_6_situacao_3' => 
                array (size=1)
                  0 => string 'atividade_6_situacao_2' (length=22)
              'atividade_6_situacao_4' => 
                array (size=2)
                  0 => string 'atividade_6_situacao_2' (length=22)
                  1 => string 'atividade_6_situacao_3' (length=22)
         */  
        $movimentacoes_for_sortable = [];
        foreach ($movimentacoes as $key => $value) {
            // se nao existir ainda insere no array
            if (!array_key_exists('atividade_' . $value['atividade_de'] . '_situacao_' . $value['situacao_de'], $movimentacoes_for_sortable)){
                foreach ($movimentacoes as $key2 => $value2) {
                    if ($value['atividade_de'] == $value2['atividade_de'] and $value['situacao_de'] == $value2['situacao_de']){
                        $movimentacoes_for_sortable['atividade_' . $value['atividade_de']  . '_situacao_' . $value['situacao_de']][] = 'atividade_' . $value2['atividade_para']  . '_situacao_' . $value2['situacao_para'];
                    }
                }
            }
        }

        $this->view->situacoes = $model_situacao->getSituacoesQuadro();
        $this->view->atividades = $model_atividade->getAtividades();
        $this->view->tarefas = $model_tarefa->getTarefasPorProjeto($projeto);
        $this->view->movimentacoes_sort = $movimentacoes_for_sortable;
        $this->view->projeto_id = $projeto;
    }

    public function salvarAction(){
        $this->_helper->removeHelper('viewRenderer');
        $request = $this->getRequest();
        $dados = $request->getParams();
        $item = explode('_',  $dados['local']);
        $atividade = $item[2];
        $situacao = $item[4];

        $tarefa = [
            'id'        => $dados['itemid'],
            'situacao'  => $situacao,
            'atividade' => $atividade
        ];

        $model_tarefa = new Model_Tarefa();
        $tarefa_inicial = $model_tarefa->getTarefa($tarefa['id']);
        $model_tarefa->updateTarefaQuadro($tarefa);

        $session = new Zend_Session_Namespace('session_kanban');
        $arr_log = [
            'id_tarefa' => $tarefa['id']
          , 'id_situacao_inicial' => $tarefa_inicial['Situação']
          , 'id_situacao_final' => $tarefa['situacao']
          , 'id_atividade_inicial' => $tarefa_inicial['Atividade']
          , 'id_atividade_final' => $tarefa['atividade']
          , 'id_autor' => $session->usuario->nome
          , 'id_apelido' => $session->usuario->id_apelido_usuario
        ];

        $model_log = new Model_LogMovimentacao();

        try{
            $model_log->addMovimentacao($arr_log);
        } catch(Exception $e){
            die($e->getMessage());
        }
    }
}
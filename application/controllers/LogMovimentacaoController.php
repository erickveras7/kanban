<?php

class LogMovimentacaoController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function indexAction()
    {
        $model_projeto  = new Model_LogMovimentacao();
        $this->view->log_movimentacoes = $model_projeto->getLogMovimentacoes();
    }
}
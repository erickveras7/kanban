<?php

class UsuariosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function indexAction()
    {
        $model_usuario  = new Model_Usuario();
        $usuarios = $model_usuario->getUsuarios();
        $this->view->usuarios = $usuarios;
    }

    public function excluirAction(){
        $request = $this->getRequest();

        if($request->isPost()) {
            $dados_post = $request->getParams();
            $model_usuario = new Model_Usuario();
            $model_usuario->excluirUsuarios($dados_post['usuario']);
            $this->view->msg = array('info', 'Usuario ' . $dados_post['usuario'] . ' excluido com sucesso');
        }
    }

    public function editarAction(){
        $request = $this->getRequest();
        $dados = $request->getParams();

        $model_usuario  = new Model_Usuario();

        if($request->isPost()) {
            
            $usuario = array(
                'apelido' => $dados['apelido'],
                'login'   => $dados['login'],
                'nome'    => $dados['nome'],
                'senha'   => $dados['senha']
            );
            
            $model_usuario = new Model_Usuario();
            $model_usuario->updateUsuario($usuario, $dados['apelido_inicial']);
            
            $this->view->msg = array('info', 'Usuario editado com sucesso');
        } else {
            $this->view->usuario = $model_usuario->getUsuario($dados['usuario']);
        }
    }

    public function adicionarAction(){
        $request = $this->getRequest();

        if($request->isPost()) {
            $dados_post = $request->getParams();

            $usuario = array(
                'apelido' => $dados_post['apelido'],
                'login'   => $dados_post['login'],
                'nome'    => $dados_post['nome'],
                'senha'   => $dados_post['senha']
            );
            
            $model_usuario = new Model_Usuario();
            $model_usuario->adicionarUsuario($usuario);
        }
        
        $this->view->msg = array('info', 'Usuario ' . $dados_post['apelido'] . ' adicionado com sucesso');
    }


}


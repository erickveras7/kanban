<?php

class Form_Login extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id', 'form-login');
        $id = new Zend_Form_Element_Hidden('id');

        $usuario = new Zend_Form_Element_Text('usuario');
        $usuario->setLabel('Usuario:')
            ->addFilter('StripTags')
            ->setAttrib('style', 'width:150px')
            ->addFilter('StringTrim');

        $senha = new Zend_Form_Element_Password('senha');
        $senha->setLabel('Senha:')
            ->addFilter('StripTags')
            ->setAttrib('style', 'width:150px')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');

        $btn_submit = new Zend_Form_Element_Submit('btn_submit', 'Login');

        $this->addElements(array($id, $usuario, $senha, $btn_submit));
    }
}



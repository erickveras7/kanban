<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initAutoload() {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->setFallbackAutoloader(true);

        if (APPLICATION_ENV == 'development' || APPLICATION_ENV == 'local') {
            $autoloader->suppressNotFoundWarnings(true);
        }

        return new Zend_Application_Module_Autoloader(array('namespace' => '', 'basePath' => APPLICATION_PATH));
    }

    protected function _initView() {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view   = $layout->getView();

        $view->doctype('XHTML1_STRICT');
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=UTF-8');
        $view->headMeta()->appendHttpEquiv('Content-Language', 'pt-BR');
        $view->headTitle('Kanban para Testes v1.0 - Erick Veras');
        $view->headTitle()->setSeparator(' :: ');

        /************
        * CSS
        ************/
        $view->headLink()->appendStylesheet('bootstrap/css/bootstrap.min.css');
        $view->headLink()->appendStylesheet('bootstrap/css/theme.bootstrap.css');

        $view->headLink()->appendStylesheet('assets/css/jquery.cluetip.css');
        $view->headLink()->appendStylesheet('jquery-ui-1.9.1/css/smoothness/jquery-ui-1.9.1.custom.min.css');
        $view->headLink()->appendStylesheet('assets/css/layout-default-latest.css');

        $view->headLink()->appendStylesheet('assets/css/layout.css');

        /************
        * JS
        ************/
        $view->headScript()->appendFile('assets/js/jquery-1.8.2.min.js');
        $view->headScript()->appendFile('assets/js/jquery.cluetip.min.js');
        $view->headScript()->appendFile('assets/js/jquery.layout-latest.min.js');

        $view->headScript()->appendFile('jquery-ui-1.9.1/js/jquery-ui-1.9.1.custom.min.js');
        $view->headScript()->appendFile('assets/js/jquery.blockUI.js');
        $view->headScript()->appendFile('assets/js/jquery.validate.js');
        $view->headScript()->appendFile('assets/js/jquery.form.js');
        
        $view->headScript()->appendFile('bootstrap/js/bootstrap.js');
        $view->headScript()->appendFile('assets/js/jquery.tablesorter.min.js');
//        $view->headScript()->appendFile('assets/js/jquery.tablesorter.widgets.js');
//        $view->headScript()->appendFile('assets/js/jquery.tablesorter.pager.js');
        $view->headScript()->appendFile('assets/js/funcoes.js');

        //views helper
        $view->addHelperPath(APPLICATION_PATH . '/../library/View/Helper/', 'View_Helper');
        return $view;
    }

    protected function _initDb() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/databases.ini', APPLICATION_ENV);

        $db_adapters = array();
        foreach ($config->db as $config_name => $db) {
            // habilitando firebug para o ambiente de desenvolvimento

            $db_adapters[$config_name] = Zend_Db::factory($db->adapter, $db->config->toArray());

            if (APPLICATION_ENV == 'development' || APPLICATION_ENV == 'local') {
                $profiler = new Zend_Db_Profiler_Firebug('All DB Queries');
                $profiler->setEnabled(true);

                $db_adapters[$config_name]->setProfiler($profiler);

//                $writer = new Zend_Log_Writer_Firebug();
//                $logger = new Zend_Log($writer);
//                Zend_Registry::set('firephp', $logger);

                //profiler de banco de dados
                $db_adapters[$config_name]->getProfiler()->setEnabled(true);
            }
        }

        Zend_Registry::set('kanban', $db_adapters);
        // seta a base da empresa como default
        Zend_Db_Table::setDefaultAdapter($db_adapters['kanban']);
    }

    protected function _initAcl() {
        $this->bootstrap('frontController');
        $frontController = $this->getResource('frontController');

        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session('kanban'));

        if (!$auth->hasIdentity()) {
            $frontController->registerPlugin(new Plugins_CheckHasAccess());
        } else {
            $aclCheck = new Plugins_AclCheck();
            $frontController->registerPlugin($aclCheck);
        }
    }

    protected function _initPlugins() {
        $this->bootstrap('frontController');
        $frontController = $this->getResource('frontController');

        $frontController->registerPlugin(new Plugins_Layout());
        //Action Helper
        Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH . '/../library/Action/Helper/', 'Action_Helper');
    }

    /**
     * Define constantes para o sistema
     */
    protected function _initConstantes() {
        $arrayConstantes = parse_ini_file(APPLICATION_PATH . '/configs/constantes.ini');

        foreach ( $arrayConstantes as $constante => $msg ) {
            define($constante, $msg);
        }
    }

    /**
     * Tradução das mensagens de validações.
     */
    public function _initTranslate() {
        $translate = new Zend_Translate('array', APPLICATION_PATH . '/configs/pt_BR.php', 'pt_BR');
        Zend_Validate_Abstract::setDefaultTranslator($translate);
    }
}

